/* eslint-disable no-unused-vars */
/* eslint-disable radix */
const { Pool } = require('pg');

const pool = new Pool({
  user: 'ndrxzckpjfsqhn',
  host: 'ec2-46-137-79-235.eu-west-1.compute.amazonaws.com',
  database: 'dccv24jbjivcam',
  password: '0642a378e7da54cb45b3c82ac68b6f2f5d62fc50b163284e5532d211b62664fb',
  port: 5432,
  ssl: { rejectUnauthorized: false },
});

const getUsers = (request, response) => {
  pool.query('SELECT * FROM player ORDER BY points DESC', (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};

const getUserById = (request, response) => {
  const { email } = request.query;
  console.log('getUserById triggered', request.query.email);

  pool.query('SELECT * FROM player WHERE email = $1', [email], (error, results) => {
    if (error) console.log('error: ', error);
    console.log('result:', results.rows);
    response.status(200).json(results.rows[0]);
  });
};
// Historia pojedynków
const getUserBattles = (request, response) => {
  const { email } = request.query;
  console.log('getUserBattles triggered', request.query.email);

  pool.query('SELECT * FROM battle WHERE winner = $1 OR looser = $1 ORDER BY id_date DESC', [email], (error, results) => {
    if (error) console.log('error: ', error);
    console.log('result:', results.rows);
    response.status(200).json(results.rows);
  });
};

const createUser = (request, response) => {
  const { email, points } = request.body;

  pool.query('INSERT INTO player (email, points, wins, battles) VALUES ($1, $2, 0, 0)', [email, points], (error, result) => {
    if (error) {
      throw error;
    }
    response.status(201).send(`User added with ID: ${result.insertId}`);
  });
};

const updateUser = (request, response) => {
  const email = parseInt(request.params.email);
  const { points } = request.body;

  pool.query(
    'UPDATE player SET points = $1 WHERE email = $2',
    [points, email],
    (error, result) => {
      if (error) {
        throw error;
      }
      response.status(200).send(`User modified with email: ${email}`);
    },
  );
};

const deleteUser = (request, response) => {
  const email = parseInt(request.params.email);

  pool.query('DELETE FROM player WHERE email = $1', [email], (error, result) => {
    if (error) {
      throw error;
    }
    response.status(200).send(`User deleted with email: ${email}`);
  });
};

module.exports = {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
  getUserBattles,
};
