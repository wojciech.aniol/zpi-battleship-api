const express = require('express');
const cors = require('cors');
// const bodyParser = require('body-parser');
const formidableMiddleware = require('express-formidable');

const app = express();
const port = process.env.PORT || 3008;
const db = require('./queries');

app.use(cors());
app.use(formidableMiddleware());
// app.use(bodyParser.json());
// app.use(
//   bodyParser.urlencoded({
//     extended: true,
//   }),
// );

// app.use((req, res, next) => {
//   res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
//   // res.header('Access-Control-Allow-Origin', 'https://zpi-battleship-client.herokuapp.com');
//   res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//   next();
// });

// app.use((req, res, next) => {
//   const allowedOrigins = ['http://localhost:3000', 'https://zpi-battleship-client.herokuapp.com'];
//   const { origin } = req.headers;
//   if (allowedOrigins.indexOf(origin) > -1) {
//     res.setHeader('Access-Control-Allow-Origin', origin);
//   }
//   // res.header('Access-Control-Allow-Origin', 'http://127.0.0.1:8020');
//   // res.header('Access-Control-Allow-Methods', 'GET, OPTIONS');
//   // res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
//   // res.header('Access-Control-Allow-Credentials', true);
//   res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//   return next();
// });

app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' });
});

app.get('/users', db.getUsers);
app.get('/user', db.getUserById);
app.get('/user/battles', db.getUserBattles);
app.post('/users', db.createUser);
app.put('/users/:email', db.updateUser);
app.delete('/users/:email', db.deleteUser);

app.listen(port, () => {
  console.log(`App running on port ${port}.`);
});
